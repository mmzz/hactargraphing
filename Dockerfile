FROM python:3.7

ENV MONGO "10.0.0.1:27017"
ENV OPTS "--verbose"

RUN apt-get install libbz2-dev
RUN git clone https://gitlab.com/mmzz/hactargraphing

WORKDIR /hactargraphing

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

CMD /hactargraphing/START
